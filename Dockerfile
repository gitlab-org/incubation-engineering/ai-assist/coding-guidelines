FROM python:3.10-alpine

WORKDIR /config

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY configs/python/.flake8 ~/.config/flake8
COPY configs/python/mypy.ini ~/.config/mypy/config

WORKDIR /app
