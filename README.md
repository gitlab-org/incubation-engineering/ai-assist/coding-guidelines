# Coding guidelines

Coding guidelines are great for companies as they will allow developers to all write similarly styles code which makes 
it easier for developers to switch context, collaborate and hand over work.

Having those guidelines is a great first step, but maintaining and reinforcing them can be quite tricky.

This repository describes a concept that aims to solve all these issues. The goals are:

1. A single source of truth for all the config files
1. Discuss modifications to the rules in Merge Requests
1. Runners always pull the latest configuration files

This concept has the potential to extend to any tool an organization uses that uses configuration files. 


## In this repo

As this is a concept, this repository demonstrates the working for Python tools and contains:

1. Configuration files for Python linters: `flake8` and `mypy`
1. Dockerfile to build a linter base image that can be used in any project


## Usage

The intended usage is in CI, in this concept you will have to define a step to run the tools, but the functionality
could be build into the core of GitLab CI, either by:

1. A pre-build coding guidelines image available on a group or account level, using a special repository similar to 
the [profile repository](https://docs.gitlab.com/ee/user/profile/#add-details-to-your-profile-with-a-readme)
1. A similar mechanism as[GitLab Secure files](https://docs.gitlab.com/ee/ci/secure_files/)
1. A similar mechanism as described in this repository

Below is an example of using the image created in this repository in any other project:

```yaml
coding-guidelines:
  image: registry.gitlab.com/gitlab-org/incubation-engineering/ai-assist/coding-guidelines:latest
  stage: lint
  script:
    - flake8 src
    - mypy src
```
